/*! \file  scope1.c
 *
 *  \brief  Simple 2 channel logic analyzer
 *
 * PORTA bits 0 and 1 are monitored and the state displayed
 *
 *  \author jjmcd
 *  \date March 17, 2015, 5:30 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include <string.h>
#include "../include/ConfigBits.h"
#include "../include/TFT.h"
#include "../include/colors.h"
#include "../include/Delays.h"
#include "../Touch.X/Touch.h"

// Define some colors so they can be easily changed
#define BACKGROUND_COLOR BLACK
#define CHANNEL1_COLOR GREEN3
#define CHANNEL2_COLOR RED
#define CURSOR_COLOR DODGERBLUE

void delay(long);

int nButtonStatus[4];
int nButtonLeft[4] = {10, 90, 170, 250};
unsigned int nTimerConstant[4] = { 50, 500, 5000, 25000 };

//! Buffer to hold captured data
unsigned char ucData[320];

//! Vertical screen position for channel 1 high
#define Y1H 40
//! Vertical screen position for channel 1 low
#define Y1L 70
//! Vertical screen position for channel 2 high
#define Y2H 120
//! Vertical screen position for channel 2 low
#define Y2L 150

#define BUTTONBACKGROUND SADDLEBROWN
#define BUTTONACTIVE BURLYWOOD
#define BUTTONOUTLINE CHOCOLATE
#define BUTTONTEXT PERU
#define BUTTONWIDTH 60
#define BUTTONHEIGHT 30

struct _current_font
{
  uint8_t* font;
  uint8_t x_size;
  uint8_t y_size;
  uint8_t offset;
  uint8_t numchars;
};

extern struct _current_font cfont;

void drawButton(int x, int y, char *label, int active)
{
  int xoff;

  if (active)
    TFTsetColorX(BUTTONACTIVE);
  else
    TFTsetColorX(BUTTONBACKGROUND);
  TFTfillRoundRect(x, y, x + BUTTONWIDTH, y + BUTTONHEIGHT);
  TFTsetColorX(BUTTONOUTLINE);
  TFTroundRect(x, y, x + BUTTONWIDTH, y + BUTTONHEIGHT);
  TFTsetFont(DJS);
  if (active)
    TFTsetBackColorX(BUTTONACTIVE);
  else
    TFTsetBackColorX(BUTTONBACKGROUND);
  TFTsetColorX(BUTTONTEXT);
  xoff = strlen(label) * cfont.x_size / 2;
  TFTprint(label, (x + BUTTONWIDTH / 2) - xoff, y + BUTTONHEIGHT / 2 - cfont.x_size, 0);
}

/*! main - Simple 2 channel logic analyzer */
int main(int argc, char *argv[])
{
  int x, y1, y2;
  int i;
  int oldY1, oldY2;
  int Tx, Ty;

  // Prepare LEDs for satus notification
  _TRISB5 = 0;
  _LATB5 = 1;
  _TRISB7 = 0;
  _LATB7 = 1;

  // Initialize LCD and clear screen
  SetupHardware();
  TFTinit(LANDSCAPE);
  TFTsetBackColorX(BACKGROUND_COLOR);
  TFTclear();
  touchInit(LANDSCAPE);
  /******* WARNING - values for hand-wired board *********/
  touchSetCalbrationData(525L, 3419L, 3889L, 358L );

  // Set RA0 and RA1 up for inputs
  ANSELAbits.ANSA0 = 0; // RA0 analog
  CNPUAbits.CNPUA0 = 1; // RA0 weak pullup
  ANSELAbits.ANSA1 = 0; // RA1 analog
  CNPUAbits.CNPUA1 = 1; // RA1 weak pullup


  //T1CONbits.SIDL  = 1;    // Discontinue operation when device enters Idle mode
  //T1CONbits.TWDIS = 1;    // Writes to TMR1 are ignored until pending write operation completes
  //T1CONbits.TGATE = 0;    // Gated time accumulation is disabled
  //T1CONbits.TCKPS = 0;    // 1:1 prescale
  //T1CONbits.TCS   = 0;    // Internal peripheral clock
  T1CONbits.ON = 1; // Timer is enabled
  PR1 = nTimerConstant[1]; // Default to 10 us

  drawButton(10, 200, "1us", 0);
  drawButton(90, 200, "10us", 1);
  drawButton(170, 200, "100us", 0);
  drawButton(250, 200, "500us", 0);
  nButtonStatus[0] = nButtonStatus[2] = nButtonStatus[3] = 0;
  nButtonStatus[1] = 1;

  while (1)
    {
      // Wait for trigger
      while ((PORTA & 0x01))  //wait for high to low
        ;
      while (PORTA & 0x01)
        ;
      // Start counting
      TMR1 = 0;
      IFS0bits.T1IF = 0;
      // Notify sampling
      _LATB7 = 0;
      // Grab 320 samples
      for (x = 0; x < 320; x++)
        {
          // Data is 2 low order bits of PORTA
          ucData[x] = (unsigned char) PORTA & 0x03;
          // Give a little time until next sample
          while (!IFS0bits.T1IF)
            ;
          IFS0bits.T1IF = 0;
        }
      _LATB7 = 1;
      // Initialize previous positions
      oldY1 = Y1H;
      oldY2 = Y2H;
      // Display the data
      for (x = 0; x < 320; x++)
        {
          if (touchDataAvailable())
            {
              touchRead();
              Tx = touchGetX();
              Ty = touchGetY();
              if (Ty > 200)
                if (Ty < (200 + BUTTONHEIGHT))
                  {
                    for (i = 0; i < 4; i++)
                      {
                        nButtonStatus[i] = 0;
                        if (Tx > nButtonLeft[i])
                          if (Tx < (nButtonLeft[i] + BUTTONWIDTH))
                            {
                              nButtonStatus[i] = 1;
                              PR1 = nTimerConstant[i];
                            }
                      }
                    drawButton( 10, 200,   "1us", nButtonStatus[0]);
                    drawButton( 90, 200,  "10us", nButtonStatus[1]);
                    drawButton(170, 200, "100us", nButtonStatus[2]);
                    drawButton(250, 200, "500us", nButtonStatus[3]);
                  }
              /*
                                  TFTsetBackColorX(BACKGROUND_COLOR);
                                  TFTsetColorX(CURSOR_COLOR);
                                  TFTprintInt(Tx,4,220,20);
                                  TFTprintInt(Ty,4,220,40);
                                  if ( Ty > 170 )
                                    drawButton( 250,200,"1ms",  1 );
                                  else
                                    drawButton( 170,200,"100us",1 );
               */
            }
          TFTsetColorX(CURSOR_COLOR);
          TFTline(x, 80, x, 110);
          // Channel 1 new vertical position
          y1 = Y1L;
          if (ucData[x]&0x01)
            y1 = Y1H;
          // Channel 2 new vertical position
          y2 = Y2L;
          if (ucData[x]&0x02)
            y2 = Y2H;
          // Erase previous scan, if any
          TFTsetColorX(BACKGROUND_COLOR);
          TFTline(x, 19, x, 171);
          /*
                              TFTpixel(x,Y1L);
                              TFTline(x,Y1H,x,Y1L);
                              TFTpixel(x,Y2L);
                              TFTline(x,Y2H,x,Y2L);
           */
          // Draw from previous position to next position
          TFTsetColorX(CHANNEL1_COLOR);
          TFTline(x, oldY1, x, y1);
          oldY1 = y1;
          TFTpixel(x, y1);
          TFTsetColorX(CHANNEL2_COLOR);
          TFTline(x, oldY2, x, y2);
          oldY2 = y2;
          TFTpixel(x, y2);
          TFTsetColorX(BACKGROUND_COLOR);
          TFTline(x, 80, x, 110);
        }
      // Illuminate the LED to notify end of scan
      _LATB5 = 0;
      // Wait a while to take a picture
      delay(500);
      _LATB5 = 1;
    }
  return 0;
}
