### A Simple two-channel logic analyzer

**scope1.X** monitors RA0 and RA1 periodically and draws a trace on
the TFT reflecting the state of the two channels.  The trace starts
when RA0 goes low.
